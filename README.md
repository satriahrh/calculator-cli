# calculator

## Compilation

### Requirements

- [`go1.12`](https://golang.org/doc/go1.12) or later

### Script
```shell script
go build -o calculator
```

## Usage

Only support operand **integer**, operator **+** and operator **-**, within infix formula.

Following example will resulting `3`.
```shell script
./calculator 1+2
```

## Warning

Minimize the usage of whitespace. Although this following example is still valid and resulting 3
```shell script
./calculator     1   + 2   
```

But, you will got an error for this following example
```shell script
./calculator 1 2 + 3
```