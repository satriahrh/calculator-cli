package main

import (
	"fmt"
	"os"
	"strings"

	"calculator/formula"
)

func main() {
	f, err := formula.NewFromInfixString(strings.Join(os.Args[1:], " "))
	if err != nil {
		fmt.Printf("error: %v\n", err)
	} else {
		fmt.Println(f.Calculate())
	}
}
