package formula

// Calculate formula using postfix procedure
func (f Formula) Calculate() (result int) {
	operandIndex := 1
	operatorIndex := 0

	result = int(f.operands[0])
	for ; operandIndex < len(f.operands); {
		currentOperand := int(f.operands[operandIndex])
		if f.operators[operatorIndex] {
			result += currentOperand
		} else {
			result -= currentOperand
		}
		operandIndex++
		operatorIndex++
	}
	return
}
