package formula_test

import (
	"fmt"
	"testing"

	"calculator/formula"
	"github.com/stretchr/testify/assert"
)

func TestNewFromInfixString(t *testing.T) {
	t.Run("Success", func(t *testing.T) {
		for _, test := range []struct {
			input    string
			expected formula.Formula
		}{
			{"0", formula.New([]uint{0}, []bool{})},
			{"0123", formula.New([]uint{123}, []bool{})},
			{"0+0", formula.New([]uint{0, 0}, []bool{true})},
			{"123+3", formula.New([]uint{123, 3}, []bool{true})},
			{"123-3", formula.New([]uint{123, 3}, []bool{false})},
			{"123-3+123", formula.New([]uint{123, 3, 123}, []bool{false, true})},
			{"123 -3 + 123- 1", formula.New([]uint{123, 3, 123, 1}, []bool{false, true, false})},
		} {
			testName := fmt.Sprint(test.input)
			t.Run(testName, func(t *testing.T) {
				actual, err := formula.NewFromInfixString(test.input)
				if assert.NoError(t, err) {
					assert.Equal(t, test.expected, actual)
				}
			})
		}
	})
	t.Run("Failed", func(t *testing.T) {
		for _, test := range []string{
			"a",
			"+",
			"1 1",
			"1 +",
			// " 1", // no case occurred
			// "1 ", // no case occurred
		} {
			testName := fmt.Sprint(test)
			t.Run(testName, func(t *testing.T) {
				_, err := formula.NewFromInfixString(test)
				assert.Error(t, err)
			})
		}
	})
}
