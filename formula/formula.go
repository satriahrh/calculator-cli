package formula

// Formula formula
type Formula struct {
	operands  []uint
	operators []bool
}

// New create a new formula
func New(operands []uint, operators []bool) Formula {
	return Formula{
		operands:  operands,
		operators: operators,
	}
}
