package formula

var intToString = []string{"0", "1", "2", "3", "4", "5", "6", "7", "8", "9"}

func runeToString(c uint8) string {
	return intToString[c-48]
}

func stringToUint(str string) (res uint) {
	for i := range str {
		c := str[len(str)-i-1]
		basis := tenPow(i)
		res += (uint(c-48) * basis)
	}
	return
}

func tenPow(y int) uint {
	if y == 0 {
		return 1
	}
	if y == 1 {
		return 10
	}
	f0 := tenPow(y / 2)
	f1 := f0
	if y-y/2 != y/2 {
		f1 = tenPow(y - y/2)
	}
	return f0 * f1
}
