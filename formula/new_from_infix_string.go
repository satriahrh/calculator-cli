package formula

import (
	"errors"
)

// NewFromInfixString infix string to formula
func NewFromInfixString(infix string) (Formula, error) {
	var err error
	state := uint8(0)
	operands, operators := make([]string, 0), make([]bool, 0)

	for i := range infix {
		c := infix[i]
		if state, operands, operators, err = newFromInfixStringStateMachine[state](c, operands, operators); err != nil {
			return Formula{}, err
		}
	}
	if (len(operands)-1 != len(operators)) {
		return Formula{}, errors.New("number of operands and operators is not valid")
	}
	uintOperands := make([]uint, 0)
	for _, operand := range operands {
		uintOperands = append(uintOperands, stringToUint(operand))
	}
	return New(uintOperands, operators), nil
}

type newFromInfixStringStateFunc func(c uint8, operands []string, operators []bool) (nextState uint8, newOperands []string, newOperators []bool, err error)

var newFromInfixStringStateMachine = []newFromInfixStringStateFunc{
	newFromInfixStringStateFunc0,
	newFromInfixStringStateFunc1,
	newFromInfixStringStateFunc2,
	newFromInfixStringStateFunc3,
	newFromInfixStringStateFunc4,
}

func newFromInfixStringStateFunc0(c uint8, operands []string, operators []bool) (nextState uint8, newOperands []string, newOperators []bool, err error) {
	newOperands, newOperators = operands, operators
	if 48 <= c && c <= 57 {
		operands = append(operands, runeToString(c))
		nextState = 1
	} else {
		err = errors.New("invalid input, try to reduce uneeded characters")
	}
	newOperands, newOperators = operands, operators
	return
}

func newFromInfixStringStateFunc1(c uint8, operands []string, operators []bool) (nextState uint8, newOperands []string, newOperators []bool, err error) {
	if 48 <= c && c <= 57 {
		operands[len(operands)-1] += runeToString(c)
		nextState = 1
	} else if c == 32 {
		nextState = 2
	} else if c == 45 || c == 43 {
		operators = append(operators, c == 43)
		nextState = 3
	} else {
		err = errors.New("invalid input, try to reduce uneeded characters")
	}
	newOperands, newOperators = operands, operators
	return
}

func newFromInfixStringStateFunc2(c uint8, operands []string, operators []bool) (nextState uint8, newOperands []string, newOperators []bool, err error) {
	newOperands, newOperators = operands, operators
	if c == 45 || c == 43 {
		operators = append(operators, c == 43)
		nextState = 3
	} else {
		err = errors.New("invalid input, try to reduce uneeded characters")
	}
	newOperands, newOperators = operands, operators
	return
}

func newFromInfixStringStateFunc3(c uint8, operands []string, operators []bool) (nextState uint8, newOperands []string, newOperators []bool, err error) {
	newOperands, newOperators = operands, operators
	if 48 <= c && c <= 57 {
		operands = append(operands, runeToString(c))
		nextState = 1
	} else if c == 32 {
		nextState = 4
	} else {
		err = errors.New("invalid input, try to reduce uneeded characters")
	}
	newOperands, newOperators = operands, operators
	return
}

func newFromInfixStringStateFunc4(c uint8, operands []string, operators []bool) (nextState uint8, newOperands []string, newOperators []bool, err error) {
	return newFromInfixStringStateFunc0(c, operands, operators)
}
