package formula_test

import (
	"fmt"
	"testing"

	"calculator/formula"
	"github.com/stretchr/testify/assert"
)

func TestFormula_Calculate(t *testing.T) {
	for _, test := range []struct {
		formula  formula.Formula
		expected int
	}{
		{formula.New([]uint{123}, []bool{}), 123},
		{formula.New([]uint{123, 3}, []bool{true}), 126},
		{formula.New([]uint{123, 3}, []bool{false}), 120},
		{formula.New([]uint{123, 3, 2}, []bool{false, true}), 122},
		{formula.New([]uint{0, 1, 2}, []bool{true, false}), -1},
	} {
		testName := fmt.Sprint(test.formula)
		t.Run(testName, func(t *testing.T) {
			actual := test.formula.Calculate()
			assert.Equal(t, test.expected, actual)
		})
	}
}
